<?php
/**
 * Created by Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 21/08/2018
 */

namespace App\Service;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig_Environment;

/**
 * Class UserService
 * @package App\Service
 */
class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * @var FormFactoryInterface
     */
    private $form;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passEncoder;

    /**
     * @var AuthenticationUtils
     */
    private $loginUtils;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $secuCheck;

    /**
     * @var TokenStorageInterface
     */
    private $security;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Twig_Environment
     */
    private $template;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $doctrine
     * @param FormFactoryInterface $form
     * @param UserPasswordEncoderInterface $passEncoder
     * @param AuthenticationUtils $loginUtils
     * @param AuthorizationCheckerInterface $secuCheck
     * @param TokenStorageInterface $security
     * @param \Swift_Mailer $mailer
     * @param Twig_Environment $template
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(EntityManagerInterface $doctrine,
                                FormFactoryInterface $form,
                                UserPasswordEncoderInterface $passEncoder,
                                AuthenticationUtils $loginUtils,
                                AuthorizationCheckerInterface $secuCheck,
                                TokenStorageInterface $security,
                                \Swift_Mailer $mailer,
                                Twig_Environment $template,
                                TokenGeneratorInterface $tokenGenerator)
    {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->passEncoder = $passEncoder;
        $this->loginUtils = $loginUtils;
        $this->secuCheck = $secuCheck;
        $this->security = $security;
        $this->mailer = $mailer;
        $this->template = $template;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * User registration form
     * If no user in the database, then the first registered will be admin
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     * @throws \Throwable
     */
    public function registerUser(Request $request)
    {
        $count = $this->doctrine->getRepository(User::class)->findAll();
        $user = new User();

        $form = $this->form->create(RegisterType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            if (count($count) == ""){
                $user->setRoles(['ROLE_ADMIN']);
            }else {
                $user->setRoles(['ROLE_USER']);
            }

            $password = $this->passEncoder->encodePassword(
                $user,
                $user->getPassword()
            );

            // Token generate
            $token = $this->tokenGenerator->generateToken();
            $user->setToken($token);

            // Send register confirmation
            $message = (new \Swift_Message('Hello Email'))
                ->setSubject('Confirmation of registration')
                ->setFrom('myadresseemail@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->template->render(
                        'emails/registration.html.twig', [
                            'username' => $user->getUsername(),
                            'email' => $user->getEmail(),
                            'password' => $user->getPassword(),
                            'token' => $token
                        ]
                    ),
                    'text/html'
                ) ;

            $this->mailer->send($message);

            $user->setPassword($password);

            $this->doctrine->persist($user);
            $this->doctrine->flush();
        }

        return $form;

    }

    /**
     * Checks if user and token match
     * @param $username
     * @param $tokenkey
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function valideRegister($username, $tokenkey)
    {
        $valide = $this->doctrine->getRepository(User::class)->checkValideUserRegister($username, $tokenkey);

        return $valide;
    }

    /**
     * Update the field to true
     * @param $username
     * @return mixed
     */
    public function UpdateValideUser($username)
    {
        $update = $this->doctrine->getRepository(User::class)->UpdateValide($username);

        return $update;
    }

    /**
     * Checks if the user is already active
     * @param $username
     * @return mixed
     */
    public function UserCheckActive($username)
    {
        $userCheckActive = $this->doctrine->getRepository(User::class)->UserCheckActive($username);

        return $userCheckActive;
    }

    /**
     * Checks if the user is ban
     * @param $username
     * @return mixed
     */
    public function UserCheckBanned($username)
    {
        $userCheckBanned = $this->doctrine->getRepository(User::class)->UserCheckBanned($username);

        return $userCheckBanned;
    }

}