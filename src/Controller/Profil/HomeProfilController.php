<?php
/**
 * Created by Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 19/03/2019
 */

namespace App\Controller\Profil;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeProfilController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/profil", name="profil_home")
     */
    public function index()
    {
        return $this->render('profl/home.html.twig');
    }
}