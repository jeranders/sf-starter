<?php
/**
 * Created by Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 27/08/2018
 */

namespace App\Controller\Security;


use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ValideRegisterController
 * @package App\Controller\Security
 */
class ValideRegisterController extends AbstractController
{
    /**
     * @param $username
     * @param $tokenkey
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @Route("/register/valide/{username}/{tokenkey}", name="valide_register")
     */
    public function valide($username, $tokenkey, UserService $userService)
    {
        $valide_token = $userService->valideRegister($username, $tokenkey);

        $userCheck = $userService->UserCheckActive($username);

        if ($userCheck == true){
            return $this->redirectToRoute('user_active');
        }

        if ($valide_token == 0){
            return $this->redirectToRoute('error_register');
        }else{
            $userService->UpdateValideUser($username);
        }

        return $this->render('security/valide_register.html.twig');
    }
}