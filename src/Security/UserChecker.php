<?php
/**
 * Created by Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 27/08/2018
 */

namespace App\Security;

use App\Entity\User as AppUser;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker
 * @package App\Security
 */
class UserChecker implements UserCheckerInterface
{

    /**
     * @param UserInterface $user
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }
    }

    /**
     * @param UserInterface $user
     * @throws \Exception
     */
    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof AppUser) {
            return;
        }

        // Checks if the user is active.
        if (!$user->getIsActive()) {
            $ex = new DisabledException();
            $ex->setUser($user);
            throw $ex;
            //throw new \Exception("Not active user");
        }

        // Checks if the user is banned.
        if (!$user->getIsBanned() == false) {
           throw new \Exception("User is banned");
        }
    }
}