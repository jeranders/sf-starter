<?php
/**
 * Created by Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 21/08/2018
 */

namespace App\Repository;


use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Parameter;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $username
     * @param $tokenkey
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkValideUserRegister($username, $tokenkey)
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->where('u.username = :username')
            ->andWhere('u.token = :token')
            ->setParameters([
                'username' => $username,
                'token' => $tokenkey
            ])
            ->getQuery()
            ->getSingleScalarResult();
            ;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function UpdateValide($username)
    {
        return $this->createQueryBuilder('u')
            ->update()
            ->set('u.isActive', 1)
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->execute();
        ;
    }

    /**
     * Checks if the user is already active
     * @param $username
     * @return mixed
     */
    public function UserCheckActive($username)
    {
        return $this->createQueryBuilder('u')
            ->select('u.isActive')
            ->where('u.username = :username')
            ->andWhere('u.isActive = 1')
            ->setParameter('username', $username)
            ->getQuery()
            ->getResult();
        ;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function UserCheckBanned($username)
    {
        return $this->createQueryBuilder('u')
            ->select('u.isActive')
            ->where('u.username = :username')
            ->andWhere('u.isBanned = 1')
            ->setParameter('username', $username)
            ->getQuery()
            ->getResult();
        ;
    }

}