<?php
/**
 * Created by Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 21/08/2018
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class RegisterType
 * @package App\Form
 */
class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Nom d\'utilisateur',
                'required' => true,
                'constraints' => array(
                    new Length(array(
                        'min' => 3,
                        'minMessage' => 'Le nom d\'utilisateur doit comporter au moins 3 caractères',
                        'max' => 15,
                        'maxMessage' => 'Le nom d\'utilisateur doit comporter au maximum 15 caractères.'
                    )),
                    new NotBlank(array(
                        'message' => 'Nom requis'
                    )),
                    new NotNull(array(
                        'message' => 'Nom requis'
                    ))
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'required' => true,
                'constraints' => array(
                    new Email(array(
                        'message' => 'Adresse email invalide',
                        'checkMX' => true
                    )),
                    new NotBlank(array(
                        'message' => 'Email requis'
                    )),
                    new NotNull(array(
                        'message' => 'Email requis'
                    ))
                )
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Répéter le mot de passe'),
                'invalid_message' => 'Le mot de passe n\'est pas identique',
                'required' => true,
                'constraints' => array(
                    new Length(array(
                        'min' => 6,
                        'minMessage' => 'Le mot de passe doit comporter au moins 6 caractères.',
                        'max' => 255,
                        'maxMessage' => 'Le mot de passe doit comporter 255 caractères maximum.'
                    )),
                    new NotBlank(array(
                        'message' => 'Mot de passe requis'
                    )),
                    new NotNull(array(
                        'message' => 'Mot de passe requis'
                    ))
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}